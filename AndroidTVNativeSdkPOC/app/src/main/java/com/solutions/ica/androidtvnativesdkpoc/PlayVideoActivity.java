package com.solutions.ica.androidtvnativesdkpoc;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.VideoView;

public class PlayVideoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);

        final VideoView videoView = (VideoView)
                findViewById(R.id.fullscreenVideoView);

        videoView.setVideoPath(
                "http://cds.y5w8j4a9.hwcdn.net/zlivingusa/index.m3u8");

        BackMediaController mediaController = new BackMediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);

        DisplayMetrics metrics = new DisplayMetrics(); getWindowManager().getDefaultDisplay().getMetrics(metrics);
        android.widget.RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) videoView.getLayoutParams();
        params.width =  metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.leftMargin = 0;
        videoView.setLayoutParams(params);


        videoView.start();

    }
}
